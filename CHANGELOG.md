# Change log

## [0.2.6](https://gitlab.com/ColinDuquesnoy/mellowplayer-connect/tree/0.2.6) (2020-9-21)
[Full Changelog](https://gitlab.com/ColinDuquesnoy/mellowplayer-connect/compare/0.2.5...0.2.6)

**Fixed bugs:**

- Flatpak integration: does not detect mpris metadata after flatpak process crashed and was restarted [\#7](https://gitlab.com/ColinDuquesnoy/mellowplayer-connect/issues/7)

## [0.2.5](https://gitlab.com/ColinDuquesnoy/mellowplayer-connect/tree/0.2.5) (2020-9-21)
[Full Changelog](https://gitlab.com/ColinDuquesnoy/mellowplayer-connect/compare/0.2.4...0.2.5)

**Fixed bugs:**

- Cannot detect a running instance of MellowPlayer when using the latest flatpak available on flathub [\#6](https://gitlab.com/ColinDuquesnoy/mellowplayer-connect/issues/6)

## [0.2.5](https://gitlab.com/ColinDuquesnoy/mellowplayer-connect/tree/0.2.5) (2020-9-21)
[Full Changelog](https://gitlab.com/ColinDuquesnoy/mellowplayer-connect/compare/0.2.4...0.2.5)

**Fixed bugs:**

- Cannot detect a running instance of MellowPlayer when using the latest flatpak available on flathub [\#6](https://gitlab.com/ColinDuquesnoy/mellowplayer-connect/issues/6)

## [0.2.4](https://gitlab.com/ColinDuquesnoy/mellowplayer-connect/tree/0.2.4) (2020-9-13)
[Full Changelog](https://gitlab.com/ColinDuquesnoy/mellowplayer-connect/compare/0.2.3...0.2.4)

**Fixed bugs:**

- Play/Pause does not work if MellowPlayer was not used during a few minutes [\#5](https://gitlab.com/ColinDuquesnoy/mellowplayer-connect/issues/5) (Better fix)

## [0.2.3](https://gitlab.com/ColinDuquesnoy/mellowplayer-connect/tree/0.2.3) (2020-8-2)
[Full Changelog](https://gitlab.com/ColinDuquesnoy/mellowplayer-connect/compare/0.2.2...0.2.3)

**Fixed bugs:**

- Play/Pause does not work if MellowPlayer was not used during a few minutes [\#5](https://gitlab.com/ColinDuquesnoy/mellowplayer-connect/issues/5)

## [0.2.2](https://gitlab.com/ColinDuquesnoy/mellowplayer-connect/tree/0.2.2) (2020-5-28)
[Full Changelog](https://gitlab.com/ColinDuquesnoy/mellowplayer-connect/compare/0.2.1...0.2.2)

**Fixed issues:**

- Fail to start if working directory is not the root install directory #4

## [0.2.1](https://gitlab.com/ColinDuquesnoy/mellowplayer-connect/tree/0.2.1) (2020-4-25)
[Full Changelog](https://gitlab.com/ColinDuquesnoy/mellowplayer-connect/compare/0.2.0...0.2.1)

**Fixed issues:**

- "origin" is a non-standard property emitted with PropertiesChanged ColinDuquesnoy/MellowPlayer#413


## [0.2.0](https://gitlab.com/ColinDuquesnoy/mellowplayer-connect/tree/0.2.0) (2020-1-1)
[Full Changelog](https://gitlab.com/ColinDuquesnoy/mellowplayer-connect/compare/0.1.0...0.2.0)

**Implemented enhancements:**

- Ability to specify custom MellowPlayer executable [\#2](https://gitlab.com/ColinDuquesnoy/mellowplayer-connect/issues/2)
- Windows support [\#1](https://gitlab.com/ColinDuquesnoy/mellowplayer-connect/issues/1)

## [0.1.0](https://gitlab.com/ColinDuquesnoy/mellowplayer-connect/tree/0.1.0) (2019-12-30)

Initial release with basic MVP.