using System;
using System.Linq;
using MellowPlayer.Connect.Lib.LocalApplication;
using MellowPlayer.Connect.Lib.Utils.Fakes;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging.Abstractions;
using NUnit.Framework;
using Action = MellowPlayer.Connect.Lib.LocalApplication.Action;

namespace MellowPlayer.Connect.Tests.LocalApplication
{
    public class LocalProcessTests
    {
        private readonly FakeTextFileFactory _textFileFactory = new FakeTextFileFactory();
        private readonly FakeProcessFactory _processFactory = new FakeProcessFactory();

        private const string FlatpakDesktopFileContent = @"[Desktop Entry]
Version=1.0
Type=Application
Name=MellowPlayer
Exec=/usr/bin/flatpak run --branch=master --arch=x86_64 --command=MellowPlayer com.gitlab.ColinDuquesnoy.MellowPlayer
Icon=com.gitlab.ColinDuquesnoy.MellowPlayer
Terminal=false
Categories=AudioVideo;
StartupNotify=true
StartupWMClass=MellowPlayer
GenericName=Cloud music player
GenericName[fr]=Lecteur de services de musique en ligne
Comment=Cloud music integration for your desktop
Comment[fr]=Intégration des services de musique en ligne avec votre bureau
Actions=PlayPause;Next;Previous;Restore;
X-Flatpak-RenamedFrom=mellowplayer.desktop;
X-Flatpak=com.gitlab.ColinDuquesnoy.MellowPlayer

[Desktop Action PlayPause]
Exec=/usr/bin/flatpak run --branch=master --arch=x86_64 --command=MellowPlayer com.gitlab.ColinDuquesnoy.MellowPlayer --play-pause
Name=Play/Pause
Icon=media-playback-start

[Desktop Action Next]
Exec=/usr/bin/flatpak run --branch=master --arch=x86_64 --command=MellowPlayer com.gitlab.ColinDuquesnoy.MellowPlayer --next
Name=Next song
Name[fr]=Piste suivante
Icon=media-seek-forward

[Desktop Action Previous]
Exec=/usr/bin/flatpak run --branch=master --arch=x86_64 --command=MellowPlayer com.gitlab.ColinDuquesnoy.MellowPlayer --previous
Name=Previous song
Name[fr]=Piste précédente
Icon=media-seek-backward

[Desktop Action Restore]
Exec=/usr/bin/flatpak run --branch=master --arch=x86_64 --command=MellowPlayer com.gitlab.ColinDuquesnoy.MellowPlayer
Name=Restore window
Name[fr]=Restaurer la fenêtre
Icon=view-restore";

        private const string NativeDesktopFileContent = @"#!/usr/bin/env xdg-open
[Desktop Entry]
Version=1.0
Type=Application
Name=MellowPlayer
Exec=MellowPlayer
TryExec=MellowPlayer
Icon=mellowplayer
Terminal=false
Categories=AudioVideo;
StartupNotify=true
StartupWMClass=MellowPlayer
GenericName=Cloud music player
GenericName[fr]=Lecteur de services de musique en ligne
Comment=Cloud music integration for your desktop
Comment[fr]=Intégration des services de musique en ligne avec votre bureau
Actions=PlayPause;Next;Previous;Restore;

[Desktop Action PlayPause]
Exec=MellowPlayer --play-pause
Name=Play/Pause
Icon=media-playback-start

[Desktop Action Next]
Exec=MellowPlayer --next
Name=Next song
Name[fr]=Piste suivante
Icon=media-seek-forward

[Desktop Action Previous]
Exec=MellowPlayer --previous
Name=Previous song
Name[fr]=Piste précédente
Icon=media-seek-backward

[Desktop Action Restore]
Exec=MellowPlayer
Name=Restore window
Name[fr]=Restaurer la fenêtre
Icon=view-restore
";

        [SetUp]
        public void SetUp()
        {
            Environment.SetEnvironmentVariable("XDG_DATA_DIRS", "/usr/share/ubuntu:/home/colin/.local/share/flatpak/exports/share/:/var/lib/flatpak/exports/share/:/usr/local/share/:/usr/share/:/var/lib/snapd/desktop");
            FakeTextFile.ClearFileContents();
        }

        private static void SetupFlatpakDesktopFile()
        {
            FakeTextFile.SetFileContent("/var/lib/flatpak/exports/share/applications/com.gitlab.ColinDuquesnoy.MellowPlayer.desktop", FlatpakDesktopFileContent);
        }
        
        private static void SetupNativeDesktopFile()
        {
            FakeTextFile.SetFileContent("/usr/share/applications/mellowplayer.desktop", NativeDesktopFileContent);
        }

        [Test]
        public void Exists_DesktopFileNotFound_False()
        {
            var sut = CreateSut();
            
            Assert.IsFalse(sut.Exists);
            Assert.AreEqual("", sut.Path);
            Assert.AreEqual("", sut.Arguments);
        }

        private LocalProcess CreateSut()
        {
            var sut = new LocalProcess(_textFileFactory, _processFactory, NullLogger<LocalProcess>.Instance, new ConfigurationBuilder().Build());
            return sut;
        }

        [Test]
        public void Exists_NativeDesktopFileFound_True()
        {
            SetupNativeDesktopFile();
            var sut = CreateSut();
            
            Assert.IsTrue(sut.Exists);
            Assert.AreEqual("MellowPlayer", sut.Path);
            Assert.AreEqual("", sut.Arguments);
        }
        
        [Test]
        public void Exists_FlatpakDesktopFileFound_True()
        {
            SetupFlatpakDesktopFile();
            var sut = CreateSut();
            
            Assert.IsTrue(sut.Exists);
            Assert.AreEqual("/usr/bin/flatpak", sut.Path);
            Assert.AreEqual("run --branch=master --arch=x86_64 --command=MellowPlayer com.gitlab.ColinDuquesnoy.MellowPlayer ", sut.Arguments);
        }

        [Test]
        public void Start_OneProcessIsRunning()
        {
            SetupNativeDesktopFile();
            var sut = CreateSut();
            
            sut.Start();
            
            Assert.AreEqual(1, FakeProcess.RunningProcesses.Count);
            Assert.AreEqual("MellowPlayer", FakeProcess.RunningProcesses.First().Key);
        }

        [Test]
        public void Execute_Action_WaitForProcessToFinish()
        {
            SetupNativeDesktopFile();
            var sut = CreateSut();
            sut.Execute(Action.PrintVersion);
            
            Assert.AreEqual(0, FakeProcess.RunningProcesses.Count);
        }

        [TestCase(Action.PrintVersion, "--version")]
        [TestCase(Action.ToggleFavorite, "--toggle-favorite-song")]
        [TestCase(Action.TogglePlayPause, "--play-pause")]
        [TestCase(Action.GoToNext, "--next")]
        [TestCase(Action.GoToPrevious, "--previous")]
        public void Execute_Action_ProcessExecutedWithCorrectArguments(Action action, string expectedProcessArguments)
        {
            SetupNativeDesktopFile();
            var sut = CreateSut();
            
            sut.Execute(action);
            
            Assert.AreEqual(expectedProcessArguments, _processFactory.Last.Arguments);
        }
    }
}