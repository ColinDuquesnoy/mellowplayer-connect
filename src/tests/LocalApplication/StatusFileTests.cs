using MellowPlayer.Connect.Lib.LocalApplication;
using MellowPlayer.Connect.Lib.Player.Fakes;
using MellowPlayer.Connect.Lib.Utils.Fakes;
using NUnit.Framework;

namespace MellowPlayer.Connect.Tests.LocalApplication
{
    public class StatusFileTests
    {
        private bool _updated;
        private const string ValidJsonFile = @"
{
    ""currentSong"": {
        ""album"": """",
        ""artUrl"": ""https://thumbnailer.mixcloud.com/unsafe/512x512/extaudio/9/8/a/f/03dc-0afe-4f86-a989-03619519462f"",
        ""artist"": ""Beats Behind The Sun"",
        ""isFavorite"": true,
        ""title"": ""Late Night High #2""
    },
    ""isPlaying"": true,
    ""serviceName"": ""Mixcloud""
}";
        private readonly FakeTextFileFactory _textFileFactory = new FakeTextFileFactory();
        private readonly FakeFileWatcher _fileWatcher = new FakeFileWatcher();
        private readonly FakeStatusFactory _statusFactory = new FakeStatusFactory();

        [SetUp]
        public void SetUp()
        {
            FakeTextFile.ClearFileContents();
            _updated = false;
        }
        
        private IStatusFile CreateSut()
        {
            return new StatusFile(_textFileFactory, _fileWatcher, _statusFactory);
        }

        [Test]
        public void Value_FileExists_PropertiesAreCorrect()
        {
            FakeTextFile.SetFileContent(StatusFile.FilePath, ValidJsonFile);

            var sut = CreateSut();
            
            AssertPropertiesAreFilled(sut);
        }

        private static void AssertPropertiesAreFilled(IStatusFile sut)
        {
            Assert.IsTrue(sut.Value.IsPlaying);
            Assert.AreEqual("Mixcloud", sut.Value.Service);
            Assert.AreEqual("Late Night High #2", sut.Value.CurrentSong.Title);
            Assert.AreEqual("Beats Behind The Sun", sut.Value.CurrentSong.Artist);
            Assert.AreEqual("", sut.Value.CurrentSong.Album);
            Assert.AreEqual("https://thumbnailer.mixcloud.com/unsafe/512x512/extaudio/9/8/a/f/03dc-0afe-4f86-a989-03619519462f", sut.Value.CurrentSong.ArtUrl);
            Assert.IsTrue(sut.Value.CurrentSong.IsFavorite);
        }

        [Test]
        public void Value_FileDoesNotExist_PropertiesAreCorrect()
        {
            var sut = CreateSut();

            AssertPropertiesHaveTheirDefaultValues(sut);
        }

        private static void AssertPropertiesHaveTheirDefaultValues(IStatusFile sut)
        {
            Assert.IsFalse(sut.Value.IsPlaying);
            Assert.AreEqual("", sut.Value.Service);
            Assert.AreEqual("", sut.Value.CurrentSong.Title);
            Assert.AreEqual("", sut.Value.CurrentSong.Artist);
            Assert.AreEqual("", sut.Value.CurrentSong.Album);
            Assert.AreEqual("", sut.Value.CurrentSong.ArtUrl);
            Assert.IsFalse(sut.Value.CurrentSong.IsFavorite);
        }

        [Test]
        public void Value_FileExistsThenIsUpdated_PropertiesAreCorrect()
        {
            FakeTextFile.SetFileContent(StatusFile.FilePath, "");

            var sut = CreateSut();

            sut.Updated += (sender, args) => _updated = true;     
            
            FakeTextFile.SetFileContent(StatusFile.FilePath, ValidJsonFile);
            AssertPropertiesHaveTheirDefaultValues(sut);
            
            _fileWatcher.Trigger();
            AssertPropertiesAreFilled(sut);
            Assert.IsTrue(_updated);
        }
    }
}