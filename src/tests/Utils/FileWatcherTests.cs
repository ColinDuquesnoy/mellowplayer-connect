using System;
using MellowPlayer.Connect.Lib.Utils;
using MellowPlayer.Connect.Lib.Utils.Fakes;
using Microsoft.Extensions.Logging.Abstractions;
using NUnit.Framework;

namespace MellowPlayer.Connect.Tests.Utils
{
    public class FileWatcherTests
    {
        private readonly FakeTimer _timer = new FakeTimer();
        private const string FilePath = "/path/to/file/to/watch";
        private readonly FakeTextFileFactory _textFileFactory = new FakeTextFileFactory();
        private bool _changed = false;

        private FileWatcher _sut;

        [SetUp]
        public void SetUp()
        {
            FakeTextFile.SetFileContent(FilePath, "");
            _changed = false;

            _sut = new FileWatcher(_textFileFactory, _timer, NullLogger<FileWatcher>.Instance);
            _sut.Changed += OnChanged;
        }

        [TearDown]
        public void TearDown()
        {
            _timer.Stop();
        }

        private void OnChanged(object sender, EventArgs e)
        {
            _changed = true;
        }

        [Test]
        public void Watch_FileIsCreated_ChangedIsTriggered()
        {
            _sut.Watch(FilePath);

            _timer.Trigger();

            Assert.IsTrue(_changed);
        }

        [Test]
        public void Watch_FileContentIsModified_ChangedIsTriggered()
        {
            _sut.Watch(FilePath);

            FakeTextFile.SetFileContent(FilePath, "Hello World!");
            _timer.Trigger();

            Assert.IsTrue(_changed);
        }

        [Test]
        public void Watch_FileContentIsModifiedTwice_ChangedIsTriggeredOnce()
        {
            _sut.Watch(FilePath);

            FakeTextFile.SetFileContent(FilePath, "Hello World!");
            _timer.Trigger();

            Assert.IsTrue(_changed);

            _changed = false;

            _timer.Trigger();

            Assert.IsFalse(_changed);
        }

        [Test]
        public void Watch_FileIsDeleted_ChangedIsTriggered()
        {
            _sut.Watch(FilePath);

            FakeTextFile.RemoveFileContent(FilePath);
            _timer.Trigger();

            Assert.IsTrue(_changed);
        }
    }
}