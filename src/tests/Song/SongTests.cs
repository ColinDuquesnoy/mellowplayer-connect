using MellowPlayer.Connect.Lib.LocalApplication.Fakes;
using MellowPlayer.Connect.Lib.Song;
using Microsoft.Extensions.Logging.Abstractions;
using NUnit.Framework;

namespace MellowPlayer.Connect.Tests.Song
{
    public class SongTests
    {
        private readonly FakeInteractionMethod _interactionMethod;
        private readonly SongFactory _songFactory;
        private ISong _sut;

        public SongTests()
        {
            _interactionMethod = new FakeInteractionMethod();
            _songFactory = new SongFactory(NullLogger<Connect.Lib.Song.Song>.Instance);
        }

        [SetUp]
        public void SetUp()
        {
            _sut = _songFactory.Create("title", "album", "artist", "https://wwww.art-url.com", true); 
            _interactionMethod.SetCurrentSong(_sut);
        }

        [Test]
        public void PropertiesAreCorrectlyInitialized()
        {
            CheckConstantProperties();
            Assert.IsTrue(_sut.IsFavorite);
        }

        private void CheckConstantProperties()
        {
            Assert.AreEqual("title", _sut.Title);
            Assert.AreEqual("album", _sut.Album);
            Assert.AreEqual("artist", _sut.Artist);
            Assert.AreEqual("https://wwww.art-url.com", _sut.ArtUrl);
        }

        [Test]
        public void Equals_SameValue_True()
        {
            var sameSong = _songFactory.Create("title", "album", "artist", "https://wwww.art-url.com", true);
            Assert.IsTrue(_sut.Equals(sameSong));
        }
        
        [Test]     
        public void Equals_DifferentTitle_False()
        {
            var otherSong = _songFactory.Create("title2", "album", "artist", "https://wwww.art-url.com", true);
            Assert.IsFalse(_sut.Equals(otherSong));
        }
        
        [Test]     
        public void Equals_DifferentAlbum_False()
        {
            var otherSong = _songFactory.Create("title", "album2", "artist", "https://wwww.art-url.com", true);
            Assert.IsFalse(_sut.Equals(otherSong));
        }
        
        [Test]     
        public void Equals_DifferentArtist_False()
        {
            var otherSong = _songFactory.Create("title", "album", "artist2", "https://wwww.art-url.com", true);
            Assert.IsFalse(_sut.Equals(otherSong));
        }
        
        [Test]     
        public void Equals_DifferentArtUrl_False()
        {
            var otherSong = _songFactory.Create("title", "album", "artist", "https://wwww.art-url2.com", true);
            Assert.IsFalse(_sut.Equals(otherSong));
        }
        
        [Test]     
        public void Equals_NotFavorite_False()
        {
            var otherSong = _songFactory.Create("title", "album", "artist", "https://wwww.art-url.com", false);
            Assert.IsFalse(_sut.Equals(otherSong));
        }
    }
}