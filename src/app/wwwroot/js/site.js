let applicationHub = new ApplicationHub();
let playerHub = new PlayerHub();
let currentSongHub = new CurrentSongHub();

function hideSongInfo() {
    $("#lbl-artist").hide();
    $("#lbl-title").hide();
    $("#img-art-div").hide();
}

function showSongInfo() {
    $("#lbl-artist").show();
    $("#lbl-title").show();
    $("#img-art-div").show();
}

function showOrHideSongInfo() {
    if (currentSongHub.isValid()) {
        showSongInfo();
    } else {
        hideSongInfo();
    }
}

$(document).ready(function () {
    $("#div-startup").show();
    $("#div-player").hide();
    $("#div-start-app").hide();
    hideSongInfo();

    applicationHub.connect();
    playerHub.connect();
    currentSongHub.connect();

    window.addEventListener("applicationHub.connectedChanged", function () {
        if (applicationHub.connected) {
            $("#div-startup").hide();
        } else {
            $("#div-startup").show();
            $("#div-player").hide();
            $("#div-start-app").hide();
            $("#lbl-connecting").text("Connection lost, reconnecting...")
        }
    });

    window.addEventListener("applicationHub.runningChanged", function () {
        if (applicationHub.running) {
            console.log("MellowPlayer is running");
            $("#div-player").show();
            $("#div-start-app").hide();
        } else {
            console.log("MellowPlayer is not running");
            $("#div-player").hide();
            $("#div-start-app").show();
        }
    });

    window.addEventListener("applicationHub.canToggleFavoriteChanged", function () {
        if (applicationHub.canToggleFavorite) {
            $("#btn-favorite").show();
        } else {
            $("#btn-favorite").hide();
        }
    });

    window.addEventListener("playerHub.playingChanged", function () {
        if (playerHub.playing) {
            $("#btn-play-pause").removeClass('fa-play').addClass('fa-pause');
        } else {
            $("#btn-play-pause").removeClass('fa-pause').addClass('fa-play');
        }
    });

    window.addEventListener("playerHub.serviceChanged", function () {
        $("#header-title").text(playerHub.service === "" ? "MellowPlayer" : playerHub.service);
    });

    window.addEventListener("currentSongHub.changed", function () {
        $("#img-art").attr("src", currentSongHub.artUrl);
        $("#lbl-title").text(currentSongHub.title);
        $("#lbl-artist").text(currentSongHub.artist);
        showOrHideSongInfo();
    });

    window.addEventListener("currentSongHub.favoriteChanged", function () {
        if (currentSongHub.favorite) {
            $("#btn-favorite").removeClass('fa-heart-o').addClass('fa-heart');
        } else {
            $("#btn-favorite").removeClass('fa-heart').addClass('fa-heart-o');
        }
    });

    $("#btn-start").click(function () {
        hideSongInfo();
        applicationHub.start();
    });

    $("#btn-previous").click(function () {
        playerHub.previous();
    });

    $("#btn-play-pause").click(function () {
        playerHub.playPause();
    });

    $("#btn-next").click(function () {
        playerHub.next();
    });

    $("#btn-favorite").click(function () {
        currentSongHub.toggleFavorite();
    });
});