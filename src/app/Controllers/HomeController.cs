﻿using System.Diagnostics;
using MellowPlayer.Connect.App.Hubs;
using MellowPlayer.Connect.App.Models;
using MellowPlayer.Connect.Lib.LocalApplication;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace MellowPlayer.Connect.App.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly ILocalApplication _localApplication;

        public HomeController(ILogger<HomeController> logger,
            ILocalApplication localApplication,
            ApplicationHubNotifier applicationHubNotifier,
            PlayerHubNotifier playerHubNotifier,
            CurrentSongNotifier currentSongNotifier)
        {
            _localApplication = localApplication;
            _logger = logger;
        }

        public IActionResult Index()
        {
            ViewData["Version"] = "0.1.0";
            ViewData["LocalApplication.Version"] = _localApplication.Version;
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel {RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier});
        }
    }
}