using MellowPlayer.Connect.Lib.Player;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;

namespace MellowPlayer.Connect.App.Hubs
{
    public class CurrentSongNotifier
    {
        private readonly IHubContext<CurrentSongHub> _hubContext;
        private readonly ILogger<CurrentSongNotifier> _logger;
        private readonly IPlayer _player;

        public CurrentSongNotifier(ILogger<CurrentSongNotifier> logger, IPlayer player, IHubContext<CurrentSongHub> hubContext)
        {
            _logger = logger;
            _player = player;
            _hubContext = hubContext;

            _player.Status.CurrentSongChanged += (sender, args) =>
            {
                _hubContext.Clients.All.SendAsync("Changed", _player.Status.CurrentSong);
            };
        }
    }
}