using MellowPlayer.Connect.Lib.Player;
using Microsoft.AspNetCore.SignalR;

namespace MellowPlayer.Connect.App.Hubs
{
    public class PlayerHub : Hub
    {
        private readonly IPlayer _player;

        public PlayerHub(IPlayer player)
        {
            _player = player;
        }

        public void PlayPause()
        {
            _player.PlayPause();
        }

        public void Next()
        {
            _player.Next();
        }

        public void Previous()
        {
            _player.Previous();
        }

        public bool IsPlaying()
        {
            return _player.Status.IsPlaying;
        }

        public string Service()
        {
            return _player.Status.Service;
        }
    }
}