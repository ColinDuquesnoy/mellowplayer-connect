using MellowPlayer.Connect.Lib.LocalApplication;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;

namespace MellowPlayer.Connect.App.Hubs
{
    public class ApplicationHub : Hub
    {
        private readonly ILocalApplication _application;
        private readonly ILogger<ApplicationHub> _logger;

        public ApplicationHub(ILocalApplication application, ILogger<ApplicationHub> logger)
        {
            _application = application;
            _logger = logger;
        }

        public void Start()
        {
            _logger.LogTrace("Start Local MellowPlayer Instance");
            _application.Start();
        }

        public bool IsRunning()
        {
            _logger.LogTrace("Check for running");
            return _application.IsRunning;
        }

        public bool CanToggleFavorite()
        {
            return _application.CanToggleFavorite;
        }
    }
}