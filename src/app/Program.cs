﻿using System;
using System.IO;
using System.Reflection;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;

namespace MellowPlayer.Connect.App
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var assembly = Assembly.GetExecutingAssembly();
            var informationVersion = assembly.GetCustomAttribute<AssemblyInformationalVersionAttribute>().InformationalVersion;
            // Console.WriteLine($"MellowPlayer.Connect v{informationVersion}");
            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args)
        {
            return WebHost.CreateDefaultBuilder(args)
                .UseUrls("http://0.0.0.0:5000")
                .UseWebRoot(GetWebRoot())
                .UseStartup<Startup>();
        }

        private static string GetWebRoot()
        {
            var dir = AppDomain.CurrentDomain.BaseDirectory;
            
            while (!Directory.Exists(Path.Combine(dir, "wwwroot")))
            {
                var parentDir = Directory.GetParent(dir);
                if (parentDir == null)
                    return "";
                dir = parentDir.FullName;
            }

            return Path.Combine(dir, "wwwroot");

        }
    }
}