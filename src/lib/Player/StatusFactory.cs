using MellowPlayer.Connect.Lib.LocalApplication;
using MellowPlayer.Connect.Lib.Song;
using Microsoft.Extensions.Logging;

namespace MellowPlayer.Connect.Lib.Player
{
    public class StatusFactory : IStatusFactory
    {
        private readonly ISongFactory _songFactory;
        private readonly ILogger<Status> _logger;

        public StatusFactory(ISongFactory songFactory, ILogger<Status> logger)
        {
            _songFactory = songFactory;
            _logger = logger;
        }

        public IStatus Create()
        {
            return new Status(_songFactory, _logger);
        }

        public IStatus Create(IStatus other)
        {
            return new Status(_songFactory, other);
        }
    }
}