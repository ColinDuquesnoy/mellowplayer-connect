namespace MellowPlayer.Connect.Lib.Song
{
    public interface ISongFactory
    {
        ISong Create(string title, string album, string artist, string artUrl, bool isFavorite);
    }
}