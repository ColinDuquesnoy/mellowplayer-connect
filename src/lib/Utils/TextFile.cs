using System.IO;

namespace MellowPlayer.Connect.Lib.Utils
{
    public class TextFile : ITextFile
    {
        private readonly string _path;

        public TextFile(string path)
        {
            _path = path;
        }
        
        public string Read() => File.Exists(_path) ? InnerRead() : "";

        public bool Exists() => File.Exists(_path);

        private string InnerRead()
        {
            using (var r = new StreamReader(_path))
            {
                return r.ReadToEnd();
            }
        }
    }
}