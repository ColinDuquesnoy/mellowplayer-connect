using System;

namespace MellowPlayer.Connect.Lib.Utils
{
    public class Timer : ITimer
    {
        private readonly System.Timers.Timer _timer = new System.Timers.Timer();
        private Action _callback = null;

        public void Stop()
        {
            _timer.Stop();
        }

        public void Start(int interval, Action callback)
        {
            Start(interval, true, callback);
        }

        public void Start(int interval, bool autoReset, Action callback)
        {
            _callback = callback;
            
            _timer.Interval = interval;
            _timer.AutoReset = autoReset;
            _timer.Elapsed += (sender, args) => _callback();

            _timer.Start();
        }
    }
}