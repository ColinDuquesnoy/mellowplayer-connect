namespace MellowPlayer.Connect.Lib.Utils
{
    public class TextFileFactory : ITextFileFactory
    {
        public ITextFile Create(string path)
        {
            return new TextFile(path);
        }
    }
}