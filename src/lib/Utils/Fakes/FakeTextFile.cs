using System.Collections.Generic;

namespace MellowPlayer.Connect.Lib.Utils.Fakes
{
    public class FakeTextFile : ITextFile
    {
        private readonly string _path;
        
        private static readonly Dictionary<string, string> FileContents = new Dictionary<string, string>();

        public FakeTextFile(string path)
        {
            _path = path;
        }

        public static void SetFileContent(string filePath, string content)
        {
            FileContents[filePath] = content;
        }

        public static void RemoveFileContent(string filePath)
        {
            FileContents.Remove(filePath);
        }

        public static void ClearFileContents()
        {
            FileContents.Clear();
        }
        
        public bool Exists()
        {
            return FileContents.ContainsKey(_path);
        }

        public string Read()
        {
            return Exists() ? FileContents[_path] : "";
        }
    }
}