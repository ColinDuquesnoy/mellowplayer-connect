using System;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using MellowPlayer.Connect.Lib.Utils;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace MellowPlayer.Connect.Lib.LocalApplication
{
    public class LocalProcess : ILocalProcess
    {
        private readonly ITextFileFactory _textFileFactory;
        private readonly IProcessFactory _processFactory;
        private readonly ILogger<LocalProcess> _logger;
        private readonly IConfiguration _configuration;

        public string Path { get; }
        public string Arguments { get; }
        public bool Exists { get; }
        public bool IsRunning => _processFactory.Create("MellowPlayer", "").IsRunning();

        private struct ExecLine
        {
            public string Path;
            public string Arguments;
        }

        public LocalProcess(ITextFileFactory textFileFactory, IProcessFactory processFactory, ILogger<LocalProcess> logger, IConfiguration configuration)
        {
            _textFileFactory = textFileFactory;
            _processFactory = processFactory;
            _logger = logger;
            _configuration = configuration;
            
            var process = _configuration["mellowplayer"];
            var processArguments = _configuration["mellowplayer-arguments"];

            if (process != null)
            {
                _logger.LogDebug("Using process from configuration (file or command line arguments)");
                Path = process;
                Arguments = processArguments ?? "";
                Exists = true;
            }
            else
            {
                if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
                {
                    // try to detect MellowPlayer executable path by looking at the first desktop entry found XDG_DATA_DIRS
                    _logger.LogDebug("Automatic detection of MellowPlayer executable by looking add desktop entries found in XDG_DATA_DIRS");
                    try
                    {
                        var desktopEntry = FindFirstDesktopEntry();
                        var execLine = GetExecLine(desktopEntry);
                        Path = execLine.Path;
                        Arguments = execLine.Arguments;
                        Exists = true;
                        _logger.LogDebug($"Process information found in {desktopEntry}");
                    }
                    catch (FileNotFoundException)
                    {
                        Exists = false;
                        Path = "";
                        Arguments = "";
                    }
                }
                else
                {
                    // use the default install path
                    Path = @"c:\Program Files\MellowPlayer\MellowPlayer.exe";
                    Arguments = "";
                    Exists = _textFileFactory.Create(Path).Exists();
                }   
            }
            
            _logger.LogInformation($"MellowPlayer process found: {Exists}" );
            _logger.LogInformation($"MellowPlayer process path: {Path}" );
            _logger.LogInformation($"MellowPlayer process arguments: {Arguments}" );
        }

        private ExecLine GetExecLine(string desktopEntryFilePath)
        {
            var textFile = _textFileFactory.Create(desktopEntryFilePath);
            var lines = textFile.Read().Split('\n');
            foreach (var line in lines)
            {
                if (!line.StartsWith("Exec=")) 
                    continue;
                
                var tokens = line.Replace("Exec=", "").Split(" ");
                var path = tokens[0];
                var arguments = "";
                if (tokens.Length > 1)
                    arguments = $"{string.Join(" ", tokens.Reverse().Take(tokens.Length - 1).Reverse())} ";
                
                var execLine = new ExecLine { Path = path, Arguments = arguments };
                return execLine;
            }
            
            throw new FileNotFoundException($"Failed to find process info from desktop file: {desktopEntryFilePath}");
        }

        private string FindFirstDesktopEntry()
        {
            var xdgDataDirs = Environment.GetEnvironmentVariable("XDG_DATA_DIRS")?.Split(":");

            if (xdgDataDirs != null)
            {
                foreach (var xdgDataDir in xdgDataDirs)
                {
                    string[] possibleDesktopFilePaths =
                    {
                        System.IO.Path.Combine(xdgDataDir, "applications", "mellowplayer.desktop"),
                        System.IO.Path.Combine(xdgDataDir, "applications", "com.gitlab.ColinDuquesnoy.MellowPlayer.desktop"),
                    };

                    foreach (var desktopFilePath in possibleDesktopFilePaths)
                    {
                        var desktopFile = _textFileFactory.Create(desktopFilePath);
                        if (desktopFile.Exists())
                            return desktopFilePath;
                    }
                }
            }

            throw new FileNotFoundException("Failed to find desktop file");
        }

        public void Start()
        {
            if (!Exists) return;

            var lockFilePath = "/tmp/MellowPlayer.lock";
            if (File.Exists(lockFilePath))
                File.Delete(lockFilePath);
            
            _processFactory.Create(Path, Arguments).Start();
        }

        public string Execute(Action action)
        {
            if (!Exists) return "";
           
            var args = $"{ToCommandLineArguments(action)}";
            if (Arguments.Length > 0)
                args += $" {Arguments}";
            _logger.LogInformation($"Running command: {Path} {args}");
            var process = _processFactory.Create(Path, args);
            process.Start();
            process.WaitForExit();
            return process.StandardOutput;
        }

        private static string ToCommandLineArguments(Action action)
        {
            var arguments = "";
            switch (action)
            {
                case Action.TogglePlayPause:
                    arguments = "--play-pause";
                    break;
                case Action.GoToNext:
                    arguments = "--next";
                    break;
                case Action.GoToPrevious:
                    arguments = "--previous";
                    break;
                case Action.PrintVersion:
                    arguments = "--version";
                    break;
                case Action.ToggleFavorite:
                    arguments = "--toggle-favorite-song";
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(action), action, null);
            }

            return arguments;
        }
    }
}