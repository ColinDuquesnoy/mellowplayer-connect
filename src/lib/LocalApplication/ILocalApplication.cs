using System;

namespace MellowPlayer.Connect.Lib.LocalApplication
{
    public interface ILocalApplication
    {
        bool IsRunning { get; }
        bool IsAvailable { get; }
        string Version { get; }
        
        bool CanToggleFavorite { get; }

        event EventHandler RunningChanged;

        void Start();
    }
}