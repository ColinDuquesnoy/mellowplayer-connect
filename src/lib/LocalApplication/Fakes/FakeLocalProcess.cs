using System;
using System.Collections.Generic;
using Action = MellowPlayer.Connect.Lib.LocalApplication.Action;

namespace MellowPlayer.Connect.Lib.LocalApplication.Fakes
{
    public class FakeLocalProcess : ILocalProcess
    {
        public string Path { get; set; } = "MellowPlayer";
        public string Arguments { get; set; } = "";
        public bool Exists { get; set; } = true;
        public bool IsRunning { get; set; } = false;
        
        public static readonly Dictionary<Action, string> ActionOutput = new Dictionary<Action, string>();
        
        public void Start()
        {
            if (IsRunning)
                throw new InvalidOperationException("Cannot start an already running process");
            IsRunning = true;
        }

        public string Execute(Action action)
        {
            return ActionOutput.ContainsKey(action) ? ActionOutput[action] : "";
        }
    }
}